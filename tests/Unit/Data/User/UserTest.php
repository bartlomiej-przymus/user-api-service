<?php

namespace Tests\Unit\Data\User;

use App\Data\User\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function providesData(): array
    {
        return [
            ['id', 2],
            ['email', 'name.surname@test.com'],
            ['firstName', 'Name'],
            ['lastName', 'Surname'],
            ['avatar', 'https://test-image.jpg'],
            ['job', 'test'],
        ];
    }

    /**
     * @test
     * @dataProvider providesData
     */
    public function will_test_getters_and_setters(
        string $name,
        $value
    ): void {
        $user = new User();

        $return = $user->{'set' . $name}($value);

        $this->assertInstanceOf(
            User::class,
            $return
        );

        $this->assertEquals(
            $value,
            $user->{'get' . $name}()
        );
    }
}
