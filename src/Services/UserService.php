<?php

namespace App\Services;

use App\Api\ApiAdapter;
use App\Data\Mappers\MapUser;
use App\Data\User\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;

class UserService extends ApiAdapter
{
    public Client $client;

    private array $responseData;

    public int $totalPages;

    public function __construct()
    {
        $this->client = $this->getHttpClient();
    }

    public function getUser(int $id): User
    {
        $promise = $this->client->getAsync('users/' . $id);

        $promise->then(
            function ($response) {
                $this->responseData = json_decode(
                    ($response->getBody())->getContents(),
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                );
            },
            function (RequestException $requestException) {
                echo $requestException->getMessage() . "\n";
                echo $requestException->getRequest()->getMethod();
            }
        );

        $promise->wait();

        return (new MapUser)->map($this->responseData['data']);
    }

    public function getUsers(int $page = 1): array
    {
        $users = [];

        $params = [
            'query' => [
                'page' => $page
            ]
        ];

        $promise = $this->client->getAsync('users', $params);

        $promise->then(
            function (Response $response) {
                $this->responseData = json_decode(
                    ($response->getBody())->getContents(),
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                );
            },
            function (RequestException $requestException) {
                echo $requestException->getMessage() . "\n";
                echo $requestException->getRequest()->getMethod();
            }
        );

        $promise->wait();

        $this->totalPages = $this->responseData['total_pages'];

        foreach ($this->responseData['data'] as $response) {
            $users[] = (new MapUser)->map($response);
        }

        return $users;
    }

    public function addUser(string $name, string $job):int {
        $params = [
            'name' => $name,
            'job' => $job,
        ];

        $promise = $this->client->postAsync('users', $params);

        $promise->then(
            function (Response $response) {
                $this->responseData = json_decode(
                    ($response->getBody())->getContents(),
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                );
            },
            function (RequestException $requestException) {
                echo $requestException->getMessage() . "\n";
                echo $requestException->getRequest()->getMethod();
            }
        );

        return $this->responseData['id'];
    }

    public function getTotalPages(): int
    {
        return $this->totalPages;
    }
}
