<?php

namespace App\Api;

use GuzzleHttp\Client as GuzzleClient;

class ApiAdapter
{
    public function getHttpClient(): GuzzleClient
    {
        return new GuzzleClient(['base_uri' => $this->getBaseUrl()]);
    }

    private function getBaseUrl(): string
    {
        return 'https://reqres.in/api/';
    }
}
