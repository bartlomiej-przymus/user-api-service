<?php

namespace App\Data\Mappers;

use App\Data\User\User;

class MapUser
{
    public function map(array $data): User
    {
        return $this->mapUser($data);
    }

    private function mapUser(array $data): User
    {
        $user = new User;

        $user->setId($data['id'])
            ->setEmail($data['email'])
            ->setFirstName($data['first_name'])
            ->setLastName($data['last_name'])
            ->setAvatar($data['avatar']);

        return $user;
    }
}
