# User Api Service

## Installation

- clone the repo
- run `composer install`

## Usage

The package has a class called UserService

that class provides 4 following methods for communicating with external APi

- getUser($id) method takes user id and outputs User model that is JSON Serializable

- getUsers($page) method has optional parameter for page number and returns
  array of User models for each page provided

- addUser($name, $job) method takes user name and user job and attempts to send that user to
  external api. If request is successful it returns id of newly added user.

- getTotalPages() method gives total number of pages of users retrieved

## Testing

Please run tests with command:
`composer run-script test`